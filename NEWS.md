# HISTORY

## Version 0.8.1

* Beta release

## Version 0.8

* First commit
* Thanksgiving Day Release (11/28/2019)
* Christmas Update (12/25/2019) to release to the public
* Updating to submit to CRAN (01/11/2020)
    + The variables updated by this package have character types, not factors.
    + New argument, rowname, is added to sail() function.
    + libsailr is updated.
* Change following the libsailr API update. (01/19/2020)
* cleanup script is updated (2/4/2020)
* Resolve warnings for submitting CRAN (2/5/2020)
* Resolve windows compilation failures. (avoid autotools dependency during installation) (2/8/2020)
* configure.ac updates to cope with autotools nonexistent system. (2/9/2020)


## Version 0.0 (Birth)

* Package skeleton was created. (11/15/2018)
   + The original name was RCppCalc, which was intended only for arithmetic calculation.


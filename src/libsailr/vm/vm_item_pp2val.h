#ifndef VM_ITEM_PP2VAL_H
#define VM_ITEM_PP2VAL_H

#include "vm_stack.h"

int stack_item_pp2value(stack_item* item);

#endif

#ifndef VM_LABEL_H
#define VM_LABEL_H

char* new_vm_label();
char* current_vm_label();

#endif /* VM_LABEL_H */
